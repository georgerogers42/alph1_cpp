#include <alph1/alph1.hpp>
#include <unordered_map>
#include <iostream>
#include <fstream>

using namespace std;
using namespace alph1;

typedef long long Count;
typedef std::unordered_map<std::string, Count> Table;
typedef bool (*Order)(const Table::value_type *a, const Table::value_type *b);

int main(int argc, char **argv) {
  vector<string> args;
  for(int i = 0; i < argc; i++) {
    args.push_back(argv[i]);
  }
  Order cmp = 0;
  if(args.size() > 1 && args[1] == "-a") {
    cmp = by_first;
  } else {
    cmp = by_second;
  }
  string fname = "TEXT-PCE-127.txt";
  for(int i = 1; i <= 100; ++i) {
    fmt::print(cout, "# {:>22}: {:>10}\n", "Begin Iteration", i);
    Table table;
    {
      ifstream f(fname);
      if(!f) {
        fmt::print(cout, "Cannot open: {}.\n", fname);
        exit(1);
      }
      build_table(table, f);
    }
    report(cout, table, cmp);
    fmt::print(cout, "# {:>22}: {:>10}\n", "End Iteration", i);
  }
  return 0;
}
