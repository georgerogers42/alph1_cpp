all: target
	(cd target; make -j)
clean: target
	(cd target; make clean -j)
pure:
	rm -rf target
run: all
	./target/alph1
target:
	./configure $(BUILD_TYPE)
.PHONY: all, clean, pure, run
