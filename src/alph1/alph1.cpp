#include <alph1/alph1.hpp>

namespace alph1 {
  using namespace std;
  void capitalize(string &word) {
    if(word == "") {
      return;
    }
    word[0] = toupper(word[0]);
  }
}
