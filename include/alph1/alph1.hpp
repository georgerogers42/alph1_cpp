#ifndef ALPH1_ALPH1_HPP
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <regex>
#include <fmt/ostream.h>

namespace alph1 {
  template<typename P>
  bool by_first(const P *a, const P *b) {
    return a->first < b->first;
  }

  template<typename P>
  bool by_second(const P *a, const P *b) {
    return a->second < b->second;
  }

  void capitalize(std::string &word);

  template<typename T>
  void build_table(T &table, std::istream &r, const std::regex &wordsplit) {
    using namespace std;
    while(r) {
      string line;
      if(!getline(r, line)) break;
      sregex_token_iterator begin(line.cbegin(), line.cend(), wordsplit, -1), end;
      for(sregex_token_iterator i = begin; i != end; ++i) {
        string word = *i;
        capitalize(word);
        if(word.size() > 0) {
          word[0] = toupper(word[0]);
          ++table[word];
        }
      }
    }
  }
  
  template<typename T>
  void build_table(T &table, std::istream &r) {
    using namespace std;
    const static regex wordsplit(R"(\W+)");
    build_table(table, r, wordsplit);
  }

  template<typename T, typename V>
  void pair_vector(V &pairs, const T &table) {
    using namespace std;
    for(auto &p : table) {
      pairs.push_back(&p);
    }
  }

  template<typename V>
  void output(std::ostream &w, const V &vec) {
    using namespace std;
    for(typename V::value_type p : vec) {
      fmt::print(w, "{:>24}: {:>10}\n", p->first, p->second);
    }
  }

  template<typename T, typename O>
  void report(std::ostream &w, const T &table, const O &o) {
    using namespace std;
    vector<const typename T::value_type*> pairs;
    pair_vector(pairs, table);
    sort(pairs.begin(), pairs.end(), o);
    output(w, pairs);
  }
}

#define ALPH1_ALPH1_HPP
#endif
